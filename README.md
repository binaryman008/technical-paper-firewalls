# Overview of Firewalls

## Abstract

The internet presence of organizations and individuals has grown rapidly in the past two decades. The internet has brought businesses and people closer. The sharing of data and business transactions are now heavily relied on the internet. Different businesses now have a presence on the internet. The confidence of the consumer is utmost important to attract the consumer to the internet. A secure communication should take place over the internet for this we use different measures to prevent malicious content to enter the computer and firewall is one such measure.

## Introduction

A firewall is a software or a piece of hardware that prevents hackers, viruses and other malicious content to enter our computer over the internet.
A software firewall is a program installed on the device while a hardware firewall is a device placed between a trusted network and untrusted network in this case, the internet. Hardware firewall protects a network of computers from the internet. A software firewall is installed on each computer so that if one computer is infected in the network other computers can be safe. The basic task of a firewall is to regulate the flow of traffic between the computer network.
If the firewall is turned off the data travels freely over the internet and hence virus or hackers can snoop into the computers very easily.

## Working of a firewall

The firewall protects the computer by applying the methodologies described below :

### 1. Packet Filtering
Over the internet, the computer establishes a connection with the other computer by entering the URL. There may be a hacker who has access to the network too.
So the firewall maintains an access list which contains a table of allowed and not allowed computers. So only that computer can send and receive the data packets which the user has requested to. When a firewall uses packet filtering, the packets entering the network are run against a group of filters.

### 2. Stateful Inspection
Firewall keeps track of which website user is currently using. The firewall maintains a conversation list containing the name of the computer and name of the website user is visiting. If the hacker has hacked the connection and is trying to communicate with the user's computer the firewall will not allow since the hacker's computer name is not in the list. In stateful inspection, the packet headers are examined and only allowed if that matches the trusted information.

### 3. Proxy Service
This firewall is incredibly secure but is slower than other firewalls. It prevents the direct connection between the user and the network by creating a mirror.
The other end of the network does not know to whom it is sending the data actually because of an intermediate hence direct access is blocked.

## Limitations of a firewall

### 1. Inefficient way of verifying data packets
The firewall has a list of trusted programs and previously allowed programs. When a data packet is received, the firewall checks whether the data packet matches the trusted program and then lets data packet to pass through it. But a hacker can easily create fake data packets with a trusted IP address and pass it through the firewall.

### 2. Threat inside the network
Hardware firewall has a limitation that it cannot filter data packets which propagate through the network above which the firewall is established. So there is a threat of insider's intrusion.

### 3. Bypassing firewall through permissions
Sometimes a program gets permission from the user to bypass the firewall. This can be very dangerous as a hacker can send malicious data through that program without being detected by the firewall.

### 4. Don't have antimalware or antivirus properties
Firewall does not have a mechanism to fend off virus and malwares

## References 
1. https://www.researchgate.net/publication/2371491_An_Overview_of_Firewall_Technologies
2. https://www.solarwindsmsp.com/blog/how-do-firewalls-work
